#! /usr/bin/env python
# -*- coding: utf8 -*-
#
# Copyright (C) 2018  The Mana World 2
# Author: Jonatas N. (Jesusalva)

import datetime
date=str(datetime.date.today())
fulldate=str(datetime.datetime.utcnow().isoformat())

#date="2018-10-06"
#fulldate="2018-10-06T20:30:20.663750"

# Open file (Dt) and export to "old". (Src) is current news
dt=open("nf_main.xml", "r")
old=[]
for line in dt:
    old.append(line)
dt.close()
dt=open("nf_main.xml", "w")
src=open("../update/news.txt", "r")


# Read news and prepare header
ns=open("../update/news.txt", "r")


# Function to markup it, and strip new lines
def markup(r):
    r=r.replace('##0', '')
    r=r.replace('##1', '')
    r=r.replace('##2', '')
    r=r.replace('##3', '')
    r=r.replace('##4', '')
    r=r.replace('##5', '')
    r=r.replace('##6', '')
    r=r.replace('##7', '')
    r=r.replace('##8', '')
    r=r.replace('##9', '')
    r=r.replace('##B', '<b>')
    r=r.replace('##b', '</b>')
    r=r.replace('[@@', '<a href="')
    r=r.replace('|', '">')
    r=r.replace('@@]', '</a>')
    return r
def nn(r):
    return r.replace('\n', '')

# Write the news, and close that file
dt.write('    <entry>\n')
for i in src:
    if ('##0 Actual Release: ##1' in i):
        dt.write('\
        <title>'+nn(i.replace('##0 Actual Release: ##1',''))+'</title>\n\
        <link href="https://tmw2.org/news#'+date+'"/>\n\
        <updated>'+fulldate+'</updated>\n\
        <id>tag:tmw2.org,'+date+'</id>\n\
        <content type="html"><![CDATA[')
    else:
        dt.write('<p>'+markup(i)+'</p>\n')
dt.write("]]></content>\n    </entry>")

dt.write('\n\n')

for i in old:
    dt.write(i)

src.close()
dt.close()

# Open main file as (Dt) and read from (Ns)
dt=open("feed.xml", "w")
ns=open("nf_main.xml", "r")

# headers
dt.write('\
<?xml version="1.0" encoding="utf-8"?>\n\
<feed xmlns="http://www.w3.org/2005/Atom">\n\
    <title>TMW2</title>\n\
    <link href="https://tmw2.org/feed.xml" rel="self"/>\n\
    <link href="https://tmw2.org"/>\n\
')
dt.write('    <updated>'+fulldate+'</updated>\n')
dt.write('\
    <id>https://tmw2.org</id>\n\
    <author>\n\
        <name>TMW2 Project</name>\n\
        <email>admin@tmw2.org</email>\n\
    </author>\n\
')

# Write data
for i in ns:
    dt.write(i)

# close
dt.write('\n</feed>')

dt.close()
ns.close()

