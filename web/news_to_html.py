#! /usr/bin/env python
# -*- coding: utf8 -*-
#
# Copyright (C) 2018  The Mana World 2
# Author: Jonatas N. (Jesusalva)

import datetime
date=str(datetime.date.today())

date="2018-10-06"

# Update the INDEX
i1=open("index.bin", "r")
i2=open("1index.bin", "w")
for i in i1:
    i2.write(i)
i2.close()
i1.close()
i1=open("index.bin", "w")
i2=open("1index.bin", "r")
i1.write(date+'\n')
for i in i2:
    i1.write(i)
i2.close()
i1.close()
i1=open("index.bin", "r")

# Prepare to sort from newest to oldest
dt=open("all_news.bin", "r")
dc=open("backup.bin", "w")
for i in dt:
    dc.write(i)
dc.close()
dt.close()
dt=open("all_news.bin", "w")
dc=open("backup.bin", "r")

# Read news and prepare header
ns=open("../update/news.txt", "r")
dt.write("\n<h3><a name="+date+">"+date+"</a></h3>\n\n<font color=black>")

# Function to markup it, and strip new lines
def markup(r):
    r=r.replace('##0', '</font><font color=black>')
    r=r.replace('##1', '<font color=red>')
    r=r.replace('##2', '<font color=green>')
    r=r.replace('##3', '<font color=blue>')
    r=r.replace('##4', '<font color=gold>')
    r=r.replace('##5', '<font color=yellow>')
    r=r.replace('##6', '<font color=pink>')
    r=r.replace('##7', '<font color=purple>')
    r=r.replace('##8', '<font color=gray>')
    r=r.replace('##9', '<font color=brown>')
    r=r.replace('##B', '<b>')
    r=r.replace('##b', '</b>')
    r=r.replace('[@@', '<a href="')
    r=r.replace('|', '">')
    r=r.replace('@@]', '</a>')
    return r
def nn(r):
    return r.replace('\n', '')

# Write the news at all_news.bin, and then reload the backup. Finally, close the file
for i in ns:
    dt.write(markup(i)+'<br/>\n')
dt.write("</font><hr/>")
for i in dc:
    dt.write(i)

ns.close()
dt.close()
dc.close()

# Write the final HTML page
wp=open("news.html", "w")
dt=open("all_news.bin", "r")

wp.write("<!DOCTYPE HTML><html><head><title>Server News</title></head><body>")
wp.write("<table border=1 align=left><tr><td>Archive</td></tr>")
for i in i1:
    wp.write('<tr><td><a href=#'+nn(i)+'>'+nn(i)+'</a></td></tr>')
wp.write('</table><br/>')

for line in dt:
    wp.write(nn(line))
wp.write("</body></html>")

wp.close()
dt.close()
i1.close()

