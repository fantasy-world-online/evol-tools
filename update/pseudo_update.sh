#!/usr/bin/env bash

# Copyright (C) 2010-2012  TMW2 Online
# Author: Andrei Karas (4144)

dir=`pwd`

previous=`cat commit.txt`

cd ../../client-data
head=`git log --pretty=oneline -n 1 | awk '{print $1}'`
u1=`echo ${previous} | cut -c 1-7`
u2=`echo ${head} | cut -c 1-7`
git log --name-status ${previous}..${head} | awk '/^(A|M)\t/ {print $2}' | \
grep -e "[.]\(xml\|png\|tmx\|ogg\|txt\|po\|tsx\)" | sort | uniq | \
xargs zip -X -9 -r ../tools/update/upload/FantasyWorld-${u1}..${u2}.zip

cd $dir/upload
pwd; ls -lh

sum=`adler32 FantasyWorld-${u1}..${u2}.zip | awk '{print $2}'`
echo "Update ID: ${u1}..${u2}"
echo "Checksum: ${sum}"

echo "FantasyWorld-${u1}..${u2}.zip ${sum}" >>resources2.txt
cp $dir/files/xml_header.txt resources.xml
echo "    <update type=\"data\" file=\"FantasyWorld-${u1}..${u2}.zip\" hash=\"${sum}\" />" >> resources.xml
cat $dir/files/xml_footer.txt >>resources.xml
cat $dir/files/xml_mods.txt >>resources.xml
echo '</updates>' >>resources.xml