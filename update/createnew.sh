#!/usr/bin/env bash

# Copyright (C) 2011-2012  TMW2 Online
# Author: Andrei Karas (4144)

dir=`pwd`

rm adler32
gcc -lz adler32.c -o adler32

mkdir files
mkdir upload

rm files/TMW2.zip
cd ../../client-data
find -iregex ".+[.]\(xml\|png\|tmx\|ogg\|txt\|po\|tsx\)" -exec touch --date=2015-01-01 {} \;
find -iregex ".+[.]\(xml\|png\|tmx\|ogg\|txt\|po\|tsx\)" -printf "%P\n" | zip -X -@ ../tools/update/files/TMW2.zip
git log --pretty=oneline -n 1 | awk '{print $1}' >../tools/update/commit.txt

cd $dir/files
sum=`adler32 TMW2.zip | awk '{print $2}'`
echo "TMW2.zip ${sum}" >resources2.txt

echo '<?xml version="1.0"?>
<updates>' >xml_header.txt
touch xml_footer.txt
touch xml_mods.txt

echo "    <update type=\"data\" file=\"TMW2.zip\" hash=\"${sum}\" />" >> xml_header.txt
cp xml_header.txt resources.xml
cat xml_footer.txt >>resources.xml
cat xml_mods.txt >>resources.xml
echo '</updates>' >>resources.xml

cp TMW2.zip ../upload/
cp resources2.txt ../upload/
cp resources.xml ../upload/
cp ../news.txt ../upload/
