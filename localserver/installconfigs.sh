#!/usr/bin/env bash

export SD="../../server-data"
export CONF="$SD/conf/import"
export NPC="$SD/npc"

mkdir $CONF
cp conf/* $CONF
cp -f ${SD}/conf/channels.conf.base ${SD}/conf/channels.conf
cp -f npc/motd-* ${NPC}/commands/
cp -f npc/017-1_* ${NPC}/017-1/
cp -f npc/015-8_* ${NPC}/015-8/
cp -f npc/botcheck_* ${NPC}/botcheck/
mkdir versions
echo "4" >versions/confver
