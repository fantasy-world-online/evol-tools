#!/usr/bin/env bash

export DIR=`pwd`

# Apply beta.patch if it exists to server-data
# It will only exist on BETA SERVERS, though
if [ -e "./beta.patch" ]
  then
    echo "Apply beta.patch ........"
    cd ../../server-data
    git apply ../tools/localserver/beta.patch
    cd $DIR
    mv beta.patch .~beta.patch
    ls
    echo "........ Done."
fi

# Apply beta.patch2 if it exists to server-code
# It will only exist on BETA SERVERS, though
if [ -e "./beta.patch2" ]
  then
    echo "Apply beta.patch2 ........"
    cd ../../server-code
    git apply ../tools/localserver/beta.patch2
    cd $DIR
    mv beta.patch2 .~beta.patch2
    ls
    echo "........ Done."
fi

./checktime.sh
source ./clean.sh
./build.sh old > /dev/null
