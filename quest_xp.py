#!/usr/bin/python2.7

"""
(C) 2018 Xtreem, All rights reserved
(C) 2018 Jesusalva, All rights reserved
This is a free software under GPL v3 License

Location Criteria:
XP LOSS             +1
Escape Route        -0.15
PVP Area            +0.1
Cramped Area        +0.1
AGR Mobs Area       +0.15
Looter BOSS         +0.15

Mob Criteria:
Agressive           +0.8
Assistant           +0.5
Rare                +0.5~+2.0

Item and Gold:      1~3 (gold weights half)
Difficulty:         Ad Infinitum
"""

XPTOLVLUP=0
I=1.0
G=1.0
L=0.0
M=0.0
D=0.0

print (XPTOLVLUP/(I+G))*(L+M+D)
