#!/usr/bin/python2.7

# Setup
x=y=0
i=j=0

skills=[]

class Skill:
  def __init__(self, sid, name, icon, desc, bmp, amp, cmd, maxlv):
    self.id=sid
    self.lv=maxlv
    self.name=name
    self.icon=icon
    self.cmd=cmd

    self.desc=desc
    self.bmp=bmp
    self.amp=amp

def fillskill(sk, lv):
    sid=sk.id
    name=sk.name
    icon=sk.icon
    desc=sk.desc
    bmp=sk.bmp
    amp=sk.amp
    cmd=sk.cmd

    if lv > 0:
        lvstr='\t\t\tlevel="%d"\n' % lv
    else:
        lvstr=''
        lv=1

    msg='\
\t\t<skill\n\
\t\t\tid="%d"\n\
\t\t\tname="%s"\n\
\t\t\ticon="graphics/skills/%s.png"\n\
\t\t\tdescription="%d MP. %s"\n\
\t\t\tinvokeCmd="@sk-%s"\n\
%s\
\t\t/>\n' % (sid, name, icon, bmp+(amp*(lv-1)), desc, cmd, lvstr)
    return msg











# Declare the skills

#########################
### Transmutation Skills
#########################
skills.append(Skill(20024, "Parum", "other/parum", "Transmutate wood into stuff.",
50, 0, "parum", 0))
skills.append(Skill(20026, "Make Potion", "transmutation", "10x plushroom, 1x milk. Create potions.",
185, -5, "mkpot", 20))

#########################
### Summon Skills
#########################
skills.append(Skill(20025, "Summon Maggots", "other/kalmurk", "2x Maggot Slime.",
200, 50, "kalmurk", 4))
skills.append(Skill(20029, "Summon Dragon", "none", "4x Dragon Scale.",
500, 40, "dragokin", 5))
skills.append(Skill(20030, "Summon Slimes", "none", "20x Maggot Slime.",
300, 30, "limerizer", 5))
skills.append(Skill(20036, "Summon Snakes", "none", "1x Snake Egg.",
350, 60, "halhiss", 8))
skills.append(Skill(20037, "Summon Wolverns", "none", "5x White Fur.",
450, 45, "kalwulf", 5))
skills.append(Skill(20038, "Summon Fairies", "none", "1x Fluo Powder.",
400, 42, "fairykingdom", 5))
skills.append(Skill(20039, "Summon Yetis", "none", "1x Frozen Yeti Tear.",
375, 55, "frozenheart", 5))
skills.append(Skill(20040, "Summon Terranite", "none", "1x Terranite Ore.",
475, 55, "stoneheart", 5))
skills.append(Skill(20041, "Summon Mouboo", "none", "1x Mouboo Figurine.",
250, 55, "kalboo", 5))
skills.append(Skill(20042, "Summon Spiky", "none", "1x Mushroom Spores.",
250, 55, "kalspike", 5))
skills.append(Skill(20043, "Summon Fluffies", "none", "1x White Fur.",
235, 45, "cuteheart", 5))
skills.append(Skill(20044, "Summon Plants", "none", "2x Artichoke Herb.",
300, 30, "plantkingdom", 5))

skills.append(Skill(20023, "Summon Cave Maggot", "none", "Req. Zarkor Scroll.",
400, 75, "zarkor", 3))












# Begin
f=open("skills.tmp", "w")

f.write('<?xml version="1.0" encoding="utf-8"?>\n<!-- This file is generated automatically, editing it will have no effect.\n     (C) Jesusalva, 2019 -->\n<skills>\n\t<set name="Mana Magic">\n')

for sk in skills:
    i=0
    while (i < sk.lv):
        i+=1
        f.write(fillskill(sk, i))

    # Fill the fallback
    sk.desc="+ "+str(sk.amp)+"/lv. "+sk.desc
    f.write(fillskill(sk, -1))
    f.write("\n")

# We're done
f.write('\n\t</set>\n</skills>')
f.close()

