#!/usr/bin/python2.7
# it: Initial Value
# v: Basic Increment
# i: Initial level (minus one)
# s: steps to increase (default 10)
# t: increase on each step (default 1)
# m: Maximum level
# b: Base Value Boost (raises it)
it=400
v=50
i=0
s=5
t=-1.15
b=0
m=160

# head: tabulation on each line
# h1:   header on first line, can be HPTable or SPTable
# tail: Comment after each line, will be followed by the level range
head="			  "
h1="	HPTable:[ "
tail="	// "

# The code begins here (bf: buffer)
bf=""
print ""
while (i <= m):
    i+=1
    if i==1:
        bf+=h1
    elif i % 10 == 1:
        bf+=tail
        bf+=str(i-10)
        bf+=" - "
        bf+=str(i-1)
        print bf
        bf=""
        bf+=head
    if (i % s == 0):
        v+=t
        it+=b
    bf+=str(int(it))
    bf+=", "
    it+=v

# Everything was printed to stdout
