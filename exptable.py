#!/usr/bin/python
# This script file is meant to create EXP table
# It have some randomness (see the import)
# It starts at level 60 (and i is the according xp)
# It stops at level 999, or when MAX_INT is reached.
import random
i=365914
l=60
o=open("expgroup.db", "w")

mx=1000
b=False

while (l < mx):
    l+=1
    if (l < 90):
        i=int(i*1.1)+random.randint(-2,12)
    elif (l < 120):
        i=int(i*1.05)+random.randint(-2,12)
    elif (l < 150):
        i=int(i*1.02)+random.randint(-2,12)
    elif (l < 270):
        i=int(i*1.01)+random.randint(-2,12)
    elif (l < 450):
        i=int(i*1.006)+random.randint(-2,12)
    elif (l < 650):
        i=int(i*1.004)+random.randint(-2,12)
    else:
        i=int(i*1.002)+random.randint(-2,12)

    if (i >= pow(2,31)):
        if not b:
            print("Interruption at level %d: max int reached" % l)
            b=True
        i=pow(2,31)-1000+l
    o.write('%d,' % i)
    if l % 10 == 0:
        o.write('\n')

o.close()
